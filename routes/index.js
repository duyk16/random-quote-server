const express = require('express')
const jwt = require('jsonwebtoken')
const data = require('./data.json')

const router = express.Router();

router.post('/login', (req, res) => {
    const { email, password } = req.body
    const userId = 10010

    if (email != 'admin@gmail.com' || password != '123456') {
        return res.status(400).json({
            status: 'error',
            message: 'Email or password is not valid'
        })
    }

    let token = jwt.sign({ userId }, "THIS_IS_SECRET", {
        expiresIn: 5 * 60 // 5 minutes
    })

    return res.status(200).json({
        status: 'success',
        data: {
            userId,
            accessToken: token,
            expiresIn: 5 * 60,
        }
    })

})

router.get('/auth/random', (req, res, next) => {
    let authorization = req.headers.authorization
    if (!authorization) return res.status(401).send({ error: 'Please login' })
    try {
        let auth = authorization.split(' ')
        if (auth[0] !== 'Bearer') return res.status(401).send({ error: 'Authorize fail' })

        req.jwt = jwt.verify(auth[1], "THIS_IS_SECRET")
        return next()

    } catch (e) {
        return res.status(403).send({ error: 'Authorize fail' })
    }
}, (req, res) => {
    const quotes = data.quotes
    return res.status(200).json({
        status: 'success',
        data: {
            quote: quotes[Math.random() * quotes.length | 0]
        }
    })
})


router.get('/random', (req, res) => {
    const quotes = data.quotes
    return res.status(200).json({
        status: 'success',
        data: {
            quote: quotes[Math.random() * quotes.length | 0]
        }
    })
})
module.exports = router;
